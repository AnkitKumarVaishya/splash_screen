import 'package:flutter/material.dart';
import 'package:splash_screen/home.dart';
import 'package:splash_screen/splash_page.dart';
void main(){
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
      'home1': (context)=>Home(),

      },
      title: "Splash Screen Of Image",
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),

    );
  }
}