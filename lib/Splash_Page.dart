import 'dart:async';
import 'package:flutter/material.dart';
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}
class _SplashScreenState extends State<SplashScreen> {
  void startTimer() {
    Timer(
      Duration(seconds: 5),
      () {
        //move to the home1 route
        Navigator.of(context).pushReplacementNamed('home1');
      },
    );
  }
@override
void initState(){
super.initState();
startTimer();
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.asset("images/bird.jpg"),
        ),
      ),
    );
  }
}
